function Jasper(resources)
{
	Jasper.resources = resources;
	
}
Jasper.prototype = {
	init: function()
	{

		this.game = new Phaser.Game(350, 700, Phaser.CANVAS, 'jasper', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this });
		this.game.created = false;
	},

	preload: function()
	{
		
		this.game.scale.maxWidth = 350;
		this.game.scale.maxHeight = 700;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.load.image('phone_shell', Jasper.resources.phone_shell);
		this.game.created = false;
		this.game.stage.backgroundColor = 'ffffff';

	},

	create: function(evt)
	{
		this.game.input.touch.preventDefault = false;
		Phaser.Canvas.setTouchAction(this.game.canvas, "auto");
		if(this.game.created === false)
		{
			//this.game.world.centerX-270
			this.parent.style = Jasper.resources.textStyle_1;
			this.parent.phone_frame = this.game.add.sprite(0,0,'phone_shell');
			this.game.created = true;
			var timer = this.game.time.create(false);
			timer.add(2000, this.parent.inview,this.parent);
			timer.start();
			
			
		}
	},
		inview: function()
	{
		var message = this.game.add.text(50,320,Jasper.resources.message,this.style);
		message.alpha = 0;
		var tw = this.game.add.tween(message).to({alpha:1},1000,Phaser.Easing.Quadratic.Out);
		tw.start();
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}





